import gensim
import os, sys

def filter_file(file, word_file):

    words = set([])
    with open(word_file) as wf:
        for line in wf:
            words.update(line.split())

    new_file = file+".filtered"
    model = gensim.models.KeyedVectors.load_word2vec_format(file, binary = True)
    words = [w for w in words if w in model.vocab]
    vec_length = 300
    
    with open(new_file, "w") as out:
        out.write(str(len(words))+ " "+ str(vec_length)+"\n")
        for word in words:
            vector = model[word]
            vector_string = " ".join([str(val) for val in vector])
            out.write(word+" "+vector_string+"\n")

if __name__=="__main__":

    file = './data/GoogleNews-vectors-negative300.bin'
    # file = './data/lexvec.enwiki+newscrawl.300d.W.pos.vectors'
    word_file = 'words_to_filter.txt'

    if not os.path.isfile(file) or not os.path.isfile(word_file):
        sys.stderr.write('Invalid filename\n')
        exit(0)

    filter_file(file, word_file)