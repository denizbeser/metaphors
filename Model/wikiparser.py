import os, sys
import vsm
import numpy as np
from gensim import corpora, models, similarities
from sklearn.feature_extraction.text import CountVectorizer

def parse_wiki():
	print('Parsing wiki...')
	doc_count = float('inf')
	textpath = './wiki/OUTPUT/AA/'

	line_tuples = []
	vocab = set([])
	word_counts = {}
	doc_names = []
	for filename in os.listdir(textpath):
	    #print(filename)
	    with open(textpath+filename) as file:
	        content = file.readlines()
	        i = 0
	        while i < len(content):
	            if len(content[i])>4 and content[i][:4] == '<doc':
	                i += 2
	                title = content[i-1]
	                text = ''
	                while i < len(content) and content[i][:5] != '</doc':
	                    text = text + content[i]
	                    i += 1
	                title = title.lower()
	                text_list = text.lower().split()
	                doc_names.append(title)
	                line_tuples.append((title,text_list))
	                vocab.update(text_list)
	                vocab.add(title)
	                # for word in text_list+[title]:
	                # 	if word in word_counts:
	                # 		word_counts[word] += 1
	                # 	else:
	                # 		word_counts[word] = 1
	            i += 1
	            if len(doc_names) > doc_count: break
	vocab = list(vocab)
	vocab_to_id = dict(zip(vocab, range(0, len(vocab))))

	# term_document_matrix = vsm.create_term_document_matrix(line_tuples, doc_names, vocab)
	# term_context_matrix = vsm.create_term_context_matrix(line_tuples, vocab, context_window_size=cs)
	return vocab_to_id, doc_names, vocab, line_tuples, word_counts

# def save_obj(obj, name):
#     with open('obj/'+ name + '.pkl', 'wb') as f:
#         pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

# def load_obj(name):
#     with open('obj/' + name + '.pkl', 'rb') as f:
#         return pickle.load(f)

# def generate_ppmi_lsa(context_size):
# 	w2id, doc_names, vocab, line_tuples, word_counts = parse_wiki()
# 	# vocab_threshold = 10
# 	# line_tuples = [(tup[0],[word for word in tup[1] if word_counts[word] > vocab_threshold]) for tup in line_tuples]
# 	# vocab = [v for v in vocab if word_counts[v] > vocab_threshold]
# 	print('Generating TDM...')
# 	tdm = vsm.create_term_document_matrix(line_tuples, doc_names, vocab)
# 	print('Generating TFIDF...')
# 	tf_idf = vsm.create_tf_idf_matrix(tdm)
# 	print('Applying LSA...')
# 	id2w = {i:w for w,i in w2id.items()}
# 	lsa = models.LsiModel(tf_idf, id2word=id2w, num_topics=300)
# 	lsa.save('/simple_wiki.lsi')
# 	save_obj(w2id, 'simple_wiki_w2id.dic')
# 	print('Saved PPMI LSA.')

# def load_lsa():
# 	lsi = models.LsiModel.load('/simple_wiki.lsi')
# 	w2id = load_obj('simple_wiki_w2id.dic')
# 	vocab = [w for w in w2id.keys()]
# 	w2v = {w:lsi[i] for w,i in w2id.items()}
# 	return w2v, vocab

# if __name__ == "__main__":
# 	context_size = 1
# 	generate_ppmi_lsa(context_size)



