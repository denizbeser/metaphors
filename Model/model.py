import gensim
from gensim.models.wrappers import FastText
import time
import random
# from nltk.corpus import wordnet as wn
import numpy as np
import matplotlib.pyplot as plt
import wikiparser
# import vsm
import math
import vsmlib
import csv

start = time.time()
# Load Vecs

print('\nLoading model...')
# W2V
w2v = gensim.models.KeyedVectors.load_word2vec_format('./data/GoogleNews-vectors-negative300.bin.filtered')
# w2v = gensim.models.KeyedVectors.load_word2vec_format('./data/Wikipedia_Vectors.bin')
# w2v = gensim.models.KeyedVectors.load_word2vec_format('./data/lexvec.enwiki+newscrawl.300d.W.pos.vectors.filtered')
vocab = w2v.vocab



# VSMLIB
# vsm = vsmlib.model.load_from_dir('./data/word_linear_cbow_250d/')
# vocab = vsm.vocabulary.lst_words
# w2v = {w:vsm.get_row(w) for w in vocab}

# Glove
# def loadGloveModel(gloveFile):
#     print("Loading Glove Model")
#     f = open(gloveFile,'r')
#     model = {}
#     vocab = set([])
#     for line in f:
#         splitLine = line.split()
#         word = splitLine[0]
#         embedding = np.array([float(val) for val in splitLine[1:]])
#         model[word] = embedding
#         vocab.add(word)
#     print("Done.",len(model)," words loaded!")
#     return model, vocab

# w2v, vocab = loadGloveModel('./data/glove.6B/glove.6B.100d.txt')

dim = 300
VERSION = 'W2V'

end = time.time()
print('Setup completed in ', end-start, ' seconds.\n')

def pmi_parse_kao_features(path):
    positive = {}
    with open(path) as file:
        for line in file:
            words = [w for w in line.split()]            
            positive[words[0]] = words[1:4]
    return positive

def parse_kao_features(path):
    positive = {}
    negative = {}
    with open(path) as file:
        for line in file:
            words = [w if w in vocab else '<UNK>' for w in line.split()]
            if '<UNK>' in words: continue
            # if words[0] == '<UNK>' : continue
            # if words[1] == '<UNK>' : continue
            # if words[4] == '<UNK>' : continue
            positive[words[0]] = words[1:4]
            negative[words[0]] = words[4:]
    #print(positive, negative)
    return positive, negative

def load_kao_data(path):
    rows = []
    reader = csv.DictReader(open(path, 'r'))
    dict_list = []
    for line in reader:
        dict_list.append(line)
    # List of list of tuples
    # Each list is one line from data file
    return dict_list

def prob_results(experiments):
    pos_feat, neg_feat = parse_kao_features("./features.txt")
    print('Results:')
    feat_vals = [[],[],[]]
    feat_stds = [[],[],[]]
    results = {}
    for exp in experiments: #4 times
        utterance, goal = exp
        #man person human (person+male)
        vec = w2v['person']#add_vecs(w2v['man'], w2v['person'])
        result,stds = get_estimates(vec, pos_feat, neg_feat, utterance, goal)
        feat_vals[0].append(result[0])
        feat_vals[1].append(result[1])
        feat_vals[2].append(result[2])
        feat_stds[0].append(stds[0])
        feat_stds[1].append(stds[1])
        feat_stds[2].append(stds[2])
        results[exp] = result
    plot_probabilities(feat_vals, feat_stds)

def plot_probabilities(feat_vals, stds):
    ## the data
    f1,f2,f3 = feat_vals
    f1_std,f2_std,f3_std = stds
    fig = plt.figure()
    ax = fig.add_subplot(111)
    #print(feat_vals)
    #print(stds)
    N = 4
    ind = np.arange(N)                # the x locations for the groups
    width = 0.25                      # the width of the bars

    ## the bars
    rects1 = ax.bar(ind, f1, width, yerr=f1_std)
    rects2 = ax.bar(ind+width, f2, width, yerr=f2_std)
    rects3 = ax.bar(ind+2*width, f3, width, yerr=f3_std)

    # axes and labels
    ax.set_xlim(-width,len(ind))
    ax.set_ylim(0,1)
    ax.set_ylabel('Probability')
    ax.set_title(VERSION+' Probabilities')
    xTickMarks = [u+' '+g for u,g in experiments]
    ax.set_xticks(ind+width)
    xtickNames = ax.set_xticklabels(xTickMarks)
    plt.setp(xtickNames, rotation=10, fontsize=8)
    plt.savefig('ngm_plot.png')
    plt.show()


def word_similarity(w1,w2):
    sim = None
    if w1 in vocab and w2 in vocab:
        sim = w2v.similarity(w1, w2)
    else: print("Word is not in the vocabulary.")
    return sim

#alpha is the weight of the first vec
def add_vecs(v1,v2,a=0.5):
    return a*v1+(1-a)*v2

def vec_similarity(v1,v2):
    return np.dot(v1, v2)/(np.linalg.norm(v1)* np.linalg.norm(v2))

def similar(vec,n):
    return w2v.most_similar([vec],[], n)

def projection(v1, v2): #projection of v1 in v2
    return np.dot(v1, v2/np.linalg.norm(v1))

#Interpretation of listener
def interpret(person_vec, feat_vec, animal_vec, utterance, goal):
    #Random initial expectation
    expectation = (np.random.rand(dim)-0.5)/5
    #Select the answer
    answer_vec = animal_vec if utterance == 'metaphorical' else feat_vec

    #Is he scary?
    if goal == 'specific':
        expectation = add_vecs(expectation,feat_vec) #Expectation: focus on "scariness" components

    #Hear Answer (Utterance) - Doesn't matter whether literal or metaphor
    #if literal: #He is scary.
    #if metaphor: #He is a shark.
    interpretation = (person_vec + answer_vec + expectation)
    return interpretation

def get_estimates(vec, pos_feat, neg_feat, utterance, goal):
    feat_probs = {0:[],1:[],2:[]}
    for animal, features in pos_feat.items():
        #isolate feature
        feat_vec = w2v[features[0]]-w2v[neg_feat[animal][0]]
        animal_vec = w2v[animal]
        interpretation = interpret(vec, feat_vec, animal_vec, utterance, goal)
        
        fv1 = np.random.rand(dim)-0.5 if features[0] == '<UNK>' else w2v[features[0]]
        fv2 = np.random.rand(dim)-0.5 if features[1] == '<UNK>' else w2v[features[1]]
        fv3 = np.random.rand(dim)-0.5 if features[2] == '<UNK>' else w2v[features[2]]
        # w_pmi = [10.74488564334433, 8.60100727547146, 6.676569796045538]
        # # fv1 = scale_pmi(fv1, animal_vec, w_pmi, 0)
        # # fv2 = scale_pmi(fv2, animal_vec, w_pmi, 1)
        # # fv3 = scale_pmi(fv3, animal_vec, w_pmi, 2)
        feat_probs[0].append(probability(interpretation, fv1))
        feat_probs[1].append(probability(interpretation, fv2))
        feat_probs[2].append(probability(interpretation, fv3))
        # feat_probs[0].append(vec_similarity(interpretation, fv1))
        # feat_probs[1].append(vec_similarity(interpretation, fv2))
        # feat_probs[2].append(vec_similarity(interpretation, fv3))

    results = [np.mean(feat_probs[0]), np.mean(feat_probs[1]), np.mean(feat_probs[2])]
    stds = [np.std(feat_probs[0]), np.std(feat_probs[1]), np.std(feat_probs[2])]
    return results, stds

def scale_pmi(feature, animal, w_pmi, i):
    w = w_pmi[i] / max(w_pmi)**2
    return w * animal + feature

def produce_cooccurence(pos_feat):
    print('Co-occurence Probabilities')
    animal_words = set([])
    feature_words = set([])
    for animal, features in pos_feat.items():
        feature_words.update(features+[animal])
        animal_words.add(animal)
    count_dict = {w1:{w2:0 for w2 in feature_words} for w1 in animal_words}
    _,_, _, line_tuples,_ = wikiparser.parse_wiki()
    window = 100

    for doc,line in line_tuples:
        for i in range(len(line)):
            word = line[i]
            if word[:-1] in animal_words: #if the word is plural of an animal, cut out '-s'
                word = word[:-1]
            if word in animal_words:
                for j in range(max(0, i - window), min(len(line), i + window + 1)):
                    other = line[j]
                    if other in feature_words:
                        count_dict[word][other] += 1
    p = [0,0,0]
    lines = []
    for animal, features in pos_feat.items():
        line = 'Animal: '+ animal+ ' ' + str(count_dict[animal][animal])+'\n'
        for i in range(len(features)):
            feat = features[i]
            line += 'F'+str(i+1)+': '+feat+' '+str(count_dict[animal][feat])+'\n'
            p[i] += count_dict[animal][feat]/count_dict[animal][animal]
        lines.append(line+'\n')
    p = [probsum/len(pos_feat) for probsum in p]
    
    with open('cooccurece_rates.txt', 'w') as out:
        out.write('Overall Probabilities: \nF1: '+str(p[0])+' F2: '+str(p[1])+' F3: '+str(p[2])+'\n')
        out.write('Window size: '+ str(window)+'\n\n\n')
        for line in lines:
            out.write(line)

def produce_pmi(pos_feat):
    print('PMI')
    animal_words = set([])
    feature_words = set([])
    for animal, features in pos_feat.items():
        feature_words.update(features+[animal])
        animal_words.add(animal)
    count_dict = {w1:{w2:0 for w2 in feature_words} for w1 in feature_words}
    total_count = 0

    window = 30
    _,_, _, line_tuples,_ = wikiparser.parse_wiki()
    for _,line in line_tuples:
        total_count += len(line)
        for i in range(len(line)):
            word = line[i]
            if word[:-1] in feature_words: #if the word is plural of an animal, cut out '-s'
                word = word[:-1]
            if word in feature_words:
                for j in range(max(0, i - window), min(len(line), i + window + 1)):
                    other = line[j]
                    if other in feature_words:
                        count_dict[word][other] += 1
    pmis = [0,0,0]
    lines = []
    for animal, features in pos_feat.items():
        line = 'Animal: '+ animal+ ' ' + str(count_dict[animal][animal])+'\n'        
        for i in range(len(features)):
            feat = features[i]
            s = 10e-10
            pmi = math.log(((count_dict[animal][feat]+s)*total_count)/(count_dict[animal][animal]+s)*(count_dict[feat][feat]+s))
            line += 'F'+str(i+1)+': '+feat+' '+str(pmi)+'\n'
            pmis[i] += pmi
        lines.append(line+'\n')
    pmis = [pmisum/len(pos_feat) for pmisum in pmis]
    
    with open('PMI_results.txt', 'w') as out:
        out.write('Overall PMI Averages: \nF1: '+str(pmis[0])+' F2: '+str(pmis[1])+' F3: '+str(pmis[2])+'\n')
        out.write('Window size: '+ str(window)+'\n\n\n')
        for line in lines:
            out.write(line)


def probability(representation, attribute):
    # Rep is 'he + shark'
    # Attribute is 'scary'

    # Get distance between vectors, similar to summing features in NGM
    # If features are directer in opposite directions (sign don't match)
    filt = np.not_equal(np.sign(representation), np.sign(attribute))
    dist = sum(abs(representation[filt])) + sum(abs(attribute[filt]))
    total = sum(abs(representation)) + sum(abs(attribute))
    return (1 - dist/total)

def generate_exp_estimates(values):
    pos_feat, neg_feat = parse_kao_features("./features.txt")
    experiments = {'1':('literal', 'vague'),'3':('literal', 'specific'),'2':('metaphorical', 'vague'),'4':('metaphorical', 'specific')}
    person_vec = w2v['person']
    #Generate estimations
    estimations = {}
    for cond,a2f in values.items():
        estimations[cond] = {}
        for animal,feats in a2f.items():
            feat_estimates = []
            for i in range(3):
                feat_vec = w2v[pos_feat[animal][0]]-w2v[neg_feat[animal][0]]
                animal_vec = w2v[animal]
                utterance, goal = experiments[cond]
                interpretation = interpret(person_vec, feat_vec, animal_vec, utterance, goal)
                prob = probability(interpretation, w2v[pos_feat[animal][i]])
                feat_estimates.append(prob)
            estimations[cond][animal] = feat_estimates
    return estimations

def get_kao_values():
    data = load_kao_data('./kao_data.csv')
    pos_feat, neg_feat = parse_kao_features("./features.txt")
    print(len(pos_feat), 'items')

    p_vocab = set([item for item in list(pos_feat.keys())+[f for fs in pos_feat.values() for f in fs]])
    #filter out of vocav
    data = [trial for trial in data if all(w in p_vocab for w in [trial['animal'],trial['f1'],trial['f2'],trial['f3']])]
    # 192 items (32 metaphors x 3 features x 2 goal conditions).
    # condition to dict of animals to [feat values + count]
    values = {cond:{trial['animal']:[0,0,0,0] for trial in data} for cond in ['1','2','3','4']}
    for trial in data:
        workerid,animal,f1,f2,f3,condition,f1prob,f2prob,f3prob = trial['workerid'],trial['animal'],trial['f1'],trial['f2'],trial['f3'],trial['condition'],trial['f1prob'],trial['f2prob'],trial['f3prob']
        values[condition][animal][0] += float(f1prob)
        values[condition][animal][1] += float(f2prob)
        values[condition][animal][2] += float(f3prob)
        values[condition][animal][3] += 1

    # Average probabilities
    values = {cond:{animal:[v/values[cond][animal][-1] for v in values[cond][animal][:3]] for animal in values[cond]} for cond in values.keys()}
    return values

def plot_average_probs(values, estimations):
    # Plot Results
    # Plot averages
    handles = [] #for legend
    for condition in values.keys():    
        trans = map(list, zip(*values[condition].values()))
        val_means = [sum(l)/len(l) for l in trans]
        trans = map(list, zip(*estimations[condition].values()))
        est_means = [sum(l)/len(l) for l in trans]
        c = (int(condition)-1)*4        
        vpoints, = plt.plot([1+c,2+c,3+c], val_means, 'o', label='C'+condition+' '+'People')
        epoints, = plt.plot([1+c,2+c,3+c], est_means, 'd', label='C'+condition+' '+'Model')
        handles.append(vpoints)
        handles.append(epoints)
    plt.legend(handles=handles)
    plt.axis([0, 20, 0, 1])
    plt.ylabel('Probability')
    plt.title('Probability Averages')
    plt.show()

def plot_predictions(values, estimations):
    # Plot Results
    estimations = {cond:{i:{animal:feats[i] for animal,feats in a2f.items()} for i in range(3)} for cond,a2f in estimations.items()}
    values = {cond:{i:{animal:feats[i] for animal,feats in a2f.items()} for i in range(3)} for cond,a2f in values.items()}

    handles = [] #for legend
    for condition, f2as in values.items():    
        if condition == '2' or condition == '4':
            shape = 'o' if condition == '2' else '^'
            name = 'Vague' if condition == '2' else 'Specific'
            for i, a2f in f2as.items():
                feat_vals = [a2f[animal] for animal in a2f.keys()]
                est_vals = [estimations[condition][i][animal] for animal in a2f.keys()]
                color = 'r'
                if i == 1: color = 'g'
                if i == 2: color = 'b'
                points, = plt.plot(est_vals, feat_vals, color+shape, label=name+' '+'f:'+str(i+1))
                handles.append(points)
    plt.legend(handles=handles)
    plt.axis([0.3, 0.9, 0.3, 0.9])
    plt.xlabel('Model')
    plt.ylabel('People')
    plt.title('Individual Probability Predictions')
    plt.show()

if __name__ == "__main__":    
    
    # KAO CONDITION TO EXP MAPPING:
    # experiments = {'1':('literal', 'vague'),'3':('literal', 'specific'),'2':('metaphorical', 'vague'),'4':('metaphorical', 'specific')}
    experiments = [('literal', 'vague'),('literal', 'specific'),('metaphorical', 'vague'),('metaphorical', 'specific')]
    values = get_kao_values()
    estimations = generate_exp_estimates(values)

    # plot_average_probs(values, estimations)
    # plot_predictions(values, estimations)
    # prob_results(experiments)
    print('\nDone.\n')

