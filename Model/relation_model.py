import gensim
from gensim.models.wrappers import FastText
import time
import random
# from nltk.corpus import wordnet as wn
import numpy as np
import matplotlib.pyplot as plt
import wikiparser
# import vsm
import math
import vsmlib
import csv
from scipy.stats import linregress
import re

start = time.time()
# Load Vecs

print('\nLoading model...')
# W2V
w2v = gensim.models.KeyedVectors.load_word2vec_format('./data/GoogleNews-vectors-negative300.bin.filtered')
vocab = set(w2v.vocab)
dim = 300
VERSION = 'W2V'

end = time.time()
print('Setup completed in ', end-start, ' seconds.\n')



def load_csv(path):
    rows = []
    reader = csv.DictReader(open(path, 'r'))
    dict_list = []
    for line in reader:
        dict_list.append(line)
    # List of list of tuples
    # Each list is one line from data file
    return dict_list

#alpha is the weight of the first vec
def add_vecs(v1,v2,a=0.5):
    return a*v1+(1-a)*v2

def cos_similarity(v1,v2):
    return np.dot(v1, v2)/(np.linalg.norm(v1)* np.linalg.norm(v2))

def similar(vec,n):
    return w2v.most_similar([vec],[], n)

def projection(v1, v2): #projection of v1 in v2
    return np.dot(v1, v2/np.linalg.norm(v1))

#Interpretation of listener
def interpret(person_vec, feat_vec, animal_vec, utterance, goal):
    #Random initial expectation
    expectation = (np.random.rand(dim)-0.5)/5
    #Select the answer
    answer_vec = animal_vec if utterance == 'metaphorical' else feat_vec

    #Is he scary?
    if goal == 'specific':
        expectation = add_vecs(expectation,feat_vec) #Expectation: focus on "scariness" components

    #Hear Answer (Utterance) - Doesn't matter whether literal or metaphor
    #if literal: #He is scary.
    #if metaphor: #He is a shark.
    interpretation = (person_vec + answer_vec + expectation)
    return interpretation

def probability(v1, v2):
    # Get distance between vectors, similar to summing features in NGM
    # If features are directer in opposite directions (sign don't match)
    filt = np.not_equal(np.sign(v1), np.sign(v2))
    dist = sum(abs(v1[filt])) + sum(abs(v2[filt]))
    total = sum(abs(v1)) + sum(abs(v2))
    return (1 - dist/total)

def estimate_relation_similarity(pair1, pair2):
    # similarity = probability(w2v[pair1[1]] - w2v[pair1[0]], w2v[pair2[1]] - w2v[pair2[0]])
    # Cos similariy, used by chen
    similarity = cos_similarity(w2v[pair1[1]] - w2v[pair1[0]], w2v[pair2[1]] - w2v[pair2[0]])
    return similarity

# Chen Exp 1
def exp1(trials):
    data_map = {}
    for trial in trials:
        words = [trial['pair1_word1'],trial['pair1_word2'],trial['pair2_word1'],trial['pair2_word2']]
        if all(word in vocab for word in words) and (trial['comparison_type']=='between-subtype' or trial['comparison_type']=='within-subtype'):
            
            # Extract Relation Type
            relation_type = int(re.sub(r'[a-z]', '', trial['relation1']))
            if relation_type not in data_map: data_map[relation_type] = {'human':[], 'model':[]}

            # Analogy pairs (Tuples of words)
            pair1, pair2 = (words[0], words[1]), (words[2], words[3])
            data_map[relation_type]['human'].append(float(trial['mean_rating'])/7)
            data_map[relation_type]['model'].append(estimate_relation_similarity(pair1, pair2))

    # Calculate R values, as y axis, and plot
    y = [linregress(d['human'], d['model'])[2]for r,d in sorted(data_map.items())]
    exp1_bar_plot(y)
    return

def exp1_bar_plot(y):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    N = 10
    ind = np.arange(N)                # the x locations for the groups
    width = 0.5                    # the width of the bars
    ## the bars
    rects = ax.bar(ind, y, width)
    # axes and labels

    ax.set_xlim(-width,len(ind)-width)
    ax.set_ylim(0,1)
    ax.set_ylabel('Pearson\'s r')
    ax.set_title(VERSION+' Human vs. Model Correlation')
    xTickMarks = range(1,11)
    ax.set_xticks(ind)
    xtickNames = ax.set_xticklabels(xTickMarks)
    plt.setp(xtickNames, rotation=10, fontsize=8)
    #plt.savefig('plot.png')
    plt.show()

if __name__ == "__main__":   

    exp1_trials = load_csv('./data/chen_data/Exp1_mean_ratings.csv')  
    exp1(exp1_trials)

    #Get each data 
    print('\nDone.\n')

